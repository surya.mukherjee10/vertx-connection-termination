import socket
import sys

totalSent = 0

print '>> Sending PUT request'
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect(('localhost', 8080))

header = '\r\n'.join([
    'PUT / HTTP/1.1',
    'Host: 127.0.0.1',
    'Accept: */*',
    'Content-Length: 10000',
    'Content-Type: text/plain',
    '', ''
])
print '>> Sending header:\n' + header
s.send(header)

data = 'a' * 1000
for i in range(5):
    sent = 0
    while sent < len(data):
        sent += s.send(data[sent:])
    totalSent += len(data)

s.close()
print '>> Closed socket; sent {} bytes so far'.format(totalSent)

