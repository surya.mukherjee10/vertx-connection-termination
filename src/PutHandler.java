import io.vertx.core.http.HttpServerRequest;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.ext.web.RoutingContext;
import io.vertx.core.buffer.Buffer;

public class PutHandler
    {
    public RoutingContext ctx;
    public HttpServerRequest req;
    public HttpServerResponse res;
    public int total;

    public PutHandler(RoutingContext ctx)
        {
        this.ctx = ctx;
        }

    public void start()
        {
        req = ctx.request();
        res = ctx.response();

        res.setChunked(true);
        req.exceptionHandler(this::handleException);
        req.endHandler(this::flush);
        req.handler(this::accept);

        write("Received PUT request");
        }

    public void accept(Buffer buf)
        {
        total += buf.length();
        write("Accepted a buffer: " + buf.length() + " total " + total);
        }

    public void flush(Void v)
        {
        write("Flush; total " + total);
        res.end();
        }

    public void handleException(Throwable e)
        {
        write("Got exception " + e.getMessage());
        res.setStatusCode(500).end("Connection abruptly terminated.");
        }

    public void write(String s)
        {
        System.out.println(s);
        res.write(s + "\n");
        }
    }
