import io.vertx.core.*;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;

public class SimpleVerticle extends AbstractVerticle
    {
    public void start() throws Exception
        {
        Router router = Router.router(vertx);
        router.get().handler(this::doGet);
        router.put().handler(this::doPut);
        vertx.createHttpServer().requestHandler(router::accept).listen(8080);
        }

    public void doGet(RoutingContext ctx)
        {
        new GetHandler(ctx).start();
        }

    public void doPut(RoutingContext ctx)
        {
        new PutHandler(ctx).start();
        }

    public static void main(String[] args) throws Exception
        {
        Vertx vertx = Vertx.vertx();
        vertx.deployVerticle(new SimpleVerticle());
        }
    }
