import io.vertx.core.http.HttpServerRequest;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.ext.web.RoutingContext;

public class GetHandler
    {
    private final RoutingContext ctx;

    public GetHandler(RoutingContext ctx)
        {
        this.ctx = ctx;
        }

    public void start()
        {
        HttpServerRequest  req = ctx.request();
        HttpServerResponse res = ctx.response();

        res.end("Hello world!");
        }
    }
